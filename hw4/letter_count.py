import numpy as np
import matplotlib
matplotlib.use('Tkagg') 
import matplotlib.pyplot as plt

def get_freq(file_path = './words.txt'):      
   string = open(file_path).read()        # taking in input file

   d = dict()
   for c in string:                       # Constructing dictionary
      if c not in d:
         d[c] = 1
      else:
         d[c] += 1
   dictlist = d.items()                    

   for key,value in sorted(d.items()):     # Printing out character and frequency in alphabetical
      print(key,value)                          

   x = np.arange( len(d.keys()) )    # Bar Graph Setup
   y = list(d.values())

                                           
   plt.bar(x, y)
   plt.plot(x, y, 'k-')
   plt.xticks(x, ('./n', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V','W', 'X', 'Y', 'Z'))
   plt.ylabel('Frequency')
   plt.xlabel('Character')
   plt.title('Frequency of Characters in words.txt')
   plt.grid(color='#D3D3D3', linestyle='solid', linewidth=1)
   plt.show()
   plt.savefig("result.png", dpi=250)

if __name__ == "__main__":
      get_freq('words.txt')
