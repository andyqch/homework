========
About me
========

Career Aspirations: Senior IT Manager, Project Manager, Network Engineer, or System Admin

Software: Linux, Windows, Git

Languages: Python, Java

Spoken Languages: English, Vietnamese, Spanish
