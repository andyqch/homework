.. antaquac_website documentation master file, created by
   sphinx-quickstart on Tue May 22 13:18:08 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to antaquac_website's documentation!
============================================

This is a website for AMS 129. The purpose of this website is to display achievements, projects, resume, etc.

About me
--------------------------------------------
3rd year TIM major @ UCSC
IT intern @ Plantronics Summer 2018 - Winter 2018


.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   ./aboutme



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
