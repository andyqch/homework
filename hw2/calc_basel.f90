subroutine calc_baselfcn(threshold, numerical_soln)
    implicit none
        real(kind=8) :: real_soln
        real (kind=8), intent(in) :: threshold                ! initializing variables
        real (kind=8), intent(out) :: numerical_soln
        integer :: error,i,j
        
        use param, only: pi                                   ! calculating real_soln using param file
        real_soln = pi**2.0/6.0
        
        numberical_soln = 1                                   !initializing loop counters
        i = 1
        j = 0
        error = 10000.0
        do while(error > threshold)
           numerical_soln = numerical_soln + 1/(i*i)          ! actual calculation
           error = ABS(real_soln - numerical_soln)            ! error calculation
            if (MOD(i, 1000) == 0,) then
               j=j+1
               print *, "n =", j*1000, "    error = ", error  ! printing error message every 1000 iterations
            end if
            i = i+1
        end do
        
        if (error < threshold) then
            print *, "Error is less than threshold."          ! print statement for the end of program
        end if
        
        print *, "total number of cycles = ", i
        print *, "numerical solution = ", numerical_soln      ! final print statements
        print *, "error = ", error
        
end subroutine calc_baselfcn
       
